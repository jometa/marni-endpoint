import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { Sex } from "./Sex";

@Entity()
export class DataBalita {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  nama: string;

  @Column({ nullable: false })
  tanggalLahir: Date;

  @Column('float',{ nullable: false })
  tinggiBadan: number;

  @Column('float', { nullable: false })
  beratBadan: number;

  @Column({
    type: 'enum',
    enum: Sex,
    nullable: false
  })
  sex: Sex;
}